use strict;
use warnings;
use Benchmark qw(timethese);

my $count = shift // 5_000_000;
my $teststr = '_WmOdasDyiNgajxgfibp:60:60:websocket,xhr-polling,jsonp-polling';
timethese(
    $count,
    {
        split => 'my ($ID) = split /:/, $teststr, 2;',
        regex => 'my ($ID) = $teststr =~ m/ [^:]+ /xms;',
    }
);

__END__
           Rate split regex
split 6849315/s    --  -15%
regex 8064516/s   18%    --

Benchmark: timing 5000000 iterations of regex, split...
regex:  1 wallclock secs ( 0.56 usr +  0.00 sys =  0.56 CPU) @ 8928571.43/s (n=5000000)
split:  0 wallclock secs ( 0.83 usr +  0.00 sys =  0.83 CPU) @ 6024096.39/s (n=5000000)

Benchmark: timing 10000000 iterations of regex, split...
regex:  2 wallclock secs ( 1.27 usr +  0.00 sys =  1.27 CPU) @ 7874015.75/s (n=10000000)
split:  1 wallclock secs ( 1.59 usr +  0.00 sys =  1.59 CPU) @ 6289308.18/s (n=10000000)


