use strict;
use warnings;
use Dragonbot;
use Dragonbot::Commands;
use lib $Dragonbot::INSTALL_DIR;

my $dragonbot = Dragonbot->new(
    channel  => 'Quail',
    username => 'dragond',
    auth     => '',
    debug    => 1,
);

while (my ($comname, $com_ref) = each %Dragonbot::Commands::commands)
{
    $dragonbot->addcom($comname => $com_ref);
}

my $error = $dragonbot->connect;
die "Couldn't connect!" if defined $error;

while (my $message = $dragonbot->())
{
    ...
}
