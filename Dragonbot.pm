package Dragonbot
{
    our $VERSION = 0.001;
    use Carp            ();
    use Const::Fast     ();
    use HTTP::Tiny      ();
    use IO::Async::Loop ();
    # $INSTALLDIR will be set programmatically in the future
    Const::Fast::const our $INSTALLDIR       => "$ENV{HOME}/Workspace/Dragonbot/src/";
    Const::Fast::const my $API               => 'http://api.hitbox.tv';
    Const::Fast::const my $DEFAULT_PORT      => 80;
    Const::Fast::const my $THROTTLE_INTERVAL => 8;

    sub new
    {
        my $self = shift;
        @_ % 2 == 0 or Carp::croak 'Bad arguments to Dragonbot constructor';
        my %options = @_;
        $options{_commands} = {};
        $options{_debug} //= 0;
        $options{_throttle} = time;
        return bless \%options, $self;
    }

    sub connect
    {
        my $self = shift;
        my $HTTP_client = HTTP::Tiny->new;
        my $servers = $self->_fetch_servers($HTTP_client);
        my $server = $servers->[ rand @{$servers} ];
        undef $servers;

        my $response = ($self->{_debug})
            ? ( do "$INSTALLDIR/t/dummy-server-response.dat" )
            : ($HTTP_client->get("http://$server->{server_ip}/socket.io/1"))
            ;
        $response->{content} // return;
        my ($ID) = $response->{content} =~ m/( [^:]+ )/xms;

        # my $client = WSC->new(
        #     on_frame => sub {
        #         my ($self, $frame) = @_;
        #         print $frame;
        #     },
        # );
        #
        # my $loop = IO::Async::Loop->new;
        # $loop->add($client);
        # $client->connect(
        #     url => "ws://$server->{server_ip}/socket.io/1/websocket/$ID",
        #     host => $server->{server_ip},
        #     service => PORT,
        #     # FIXME Make on_connected join $self->{room}
        #     on_connected     => sub { print "Connected!\n" },
        #     on_connect_error => sub { die "Cannot connect - $_[-1]" },
        #     on_resolve_error => sub { die "Cannot resolve - $_[-1]" },
        # );
        #
        # $loop->loop_forever;
        # From here we need to connect to
        # "ws://$server->{server_ip}/socket.io/1/websocket/$ID"
        # From the docs: The chat protocol has changed, and messages now are
        # not simply json. There are several socket.io frameworks, or if you
        # are using raw websocket apis, you just need to adapt to the new
        # messaging scheme: http://i.imgur.com/EAZxwgD.png
    }

    sub _fetch_servers
    {
        my $self = shift;
        my $response = ($self->debug)
            ? ( do "$INSTALLDIR/t/dummy-chat-servers.dat" )
            : ( $_[0]->get("$API/chat/servers") )
            ;
        return unless $response->{success};
        my $content = delete $response->{content};
        $content = decode_json($content);
        return [ grep { $_->{server_enabled} } @{$content} ];
    }

    sub next
    {
        ...;

=pod

From here we need to receive a message frame from $self->{_client},
parse it into the appropriate data structure using
Dragonbot::Parser, then tokenize the user message field and apply
commands appropriately. I'll have to change the Parser's message
tokenizer to only take a string, which can be passed to it from
Dragonbot. For example,

  my $message = $self->{_client}->receive;
  $message     = Dragonbot::Parser::parse($message);
  my $response = Dragonbot::Parser$message->{text}

Then

  return $message, $response;

which allows

  while (my ($message, $response) = $db->next())
  {
      $db->say($response) if defined $response;
  }

or

  if (defined $response)
  {
      $message->{RESPONSE} = $response;
  }
  return $message;

which allows

  while (my $message = $db->next())
  {
      $db->say($message->{response}) if exists $message->{response};
  }

but is a bit ugly and modifies the data structure created from parsing the
message from Hitbox.tv's format.

XXX Perhaps there needs to be a way to asynchronously receive messages
and store them FIFO in an attribute, pruning the queue to keep it below a
set limit if necessary to conserve memory and warning them if their isn't
actually, you know, using the bot. Then Dragonbot->next can simply C<shift>
the next message to be dealt with whenever the user feels like it.

=cut

    }

    sub ignore
    {
        @_ >= 2 or Carp::carp 'No user specified for Dragonbot->ignore';
        my ($self, $user) = @_;

$user =~ /[-a-zA-Z0-9]/ or Carp::croak 'Invalid username given';
        $self->{_ignored_users}{$user} = 1;
    }

    sub unignore
    {
        @_ >= 2 or Carp::carp 'No user specified for Dragonbot->unignore';
        my ($self, $user) = @_;
        delete $self->{_ignored_users}{$user};
    }

    sub is_ignored
    {
        @_ >= 2 or Carp::carp 'No user specified for Dragonbot->is_ignored';
        my ($self, $user) = @_;
        return exists $self->{_ignored_users}{$user};
    }

    sub addcom
    {
        my ($self, $comname, $com_ref) = @_;
        Carp::carp 'Not enough arguments to addcom' unless defined($self) and defined($comname);
        Carp::carp("Command's name can't be a reference"), return if ref $comname;
        Carp::carp('Command must be a coderef'), return if ref $com_ref ne ref sub {};
        $self->{_commands}{$comname} = $com_ref;
    }

    sub delcom
    {
        my ($self, $comname) = @_;
        Carp::carp('Command name not specified'), return unless $comname;
        delete $self->{_commands}{$comname};
    }

    # sub _clean # Currently unused
    # {
    #     my $self = shift;
    #     if( %{$self->{_ignored_users}} == 0 ) { delete $self->{_ignored_users} }
    #     return;
    # }

    sub _process # Execute user commands
    {
        my @tokens = reverse @_;
        TOKEN: while (my ($noncommand, $command) = shift @tokens, shift @tokens)
        {
            # FIXME
            $_ // last TOKEN for $noncommand, $command;
            my $command = Commands::get_com($command);
            eval {
            $command->is_filter or Carp::croak "Something went wrong parsing\n\t@tokens\n";
            }; Carp::confess("$@ : <$command>") if $@;
            push @tokens, $command->run($noncommand);
        }
        return @tokens;
    }
    "Huh, Toothless. I could've sworn you had--";
}
__END__

=pod

=head1 NAME

Dragonbot

=head1 SYNOPSIS

  use Dragonbot;
  my $dragonbot = Dragonbot->new(
      channel => foo,
      user    => username,
      auth    => *******,
  );

  if (my $error = $dragonbot->connect) { die $error }
  $dragonbot->say("Hello, World!");
  while (my ($message, $response) = $dragonbot->next)
  {
      $dragonbot->say($response);
  }

=head1 DESCRIPTION

A chatbot for Hitbox.TV

=head1 METHODS

=over 4

=item new

Construct a Dragonbot object.

=item connect

Connect to a channel. Required before messages can be received.

=item addcom

Add a command.

=item delcom

Remove a command.

=item ignore

Ignore a user. Commands from them will not be executed. Handy if someone likes to abuse the bot.

=item unignore

Remove a user from the ignore list.

=item next

Retrieve the next message in the current channel.

=back

=head1 CONFIGURATION AND ENVIRONMENT

=head1 DEPENDENCIES

=over 4

=item *

Const::Fast

=item *

IO::Async, IO::Async::Loop

=item *

Net::Async::WebSocket::Client

=back

=head1 BUGS AND LIMITATIONS

=over 4

=item *

It's not done yet.

=item *

It's not a proper module yet.

=item *

It doesn't reconnect after DCing.

=item *

Each Dragonbot instance can only be in one channel at a time.

=back

=head1 VERSION

=head1 AUTHOR

=head1 LICENSE AND COPYRIGHT

=head1 SEE ALSO

L<http://www.hitbox.tv/team/bots>

=cut
