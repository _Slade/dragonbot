# XXX This package will be removed; its funtionality is simple enough to add to Dragonbot.pm
package Counter
{
    use strict;
    use warnings;

    use Dragonbot ();
    our $VERSION = $Dragonbot::VERSION;

    use Carp ();
    use YAML::XS ();

    
    my $STORAGE_FILE;

    sub new
    {
        my $self = shift;
        @_ or Carp::croak(
            sprintf '%s->new() requires a storage file', $self
        );
        $STORAGE_FILE = shift;
        return bless( YAML::XS::LoadFile($STORAGE_FILE), $self );
    }
}
