package Parser
{
    use strict;
    use warnings;
    use Carp     ();
    use List::Util qw(any);
    use experimental 'postderef';

    sub parse_message
    {
        ...
    }

    sub tokenize
    {
        my $message = shift;
        ref $message eq ref {} or Carp::croak('Did not receive a hash reference');
        my $text = $message->{text} // return;

        # my $first_word = $text =~ /(\b\S+\b)/;
        # if (Commands::is_com($first_word))
        # {
        #     my $command = Commands::get_com($first_word);
        #     $command->is_static and return $command->run;
        # }
        # undef $first_word;
        return unless any { 1 + index $text, $_ } qw(fenris smasho);

        my @tokens;
        while ($text =~ m{ !(?:fenris|smasho)\b }xms)
        {
            my $noncommand = substr($text, 0, $-[0], '');
            substr($text, 0, 1, '');
            my $command = substr($text, 0, $+[0] - $-[0] - 1, '');
            push @tokens, $noncommand, $command;
        }
        push @tokens, $text;
        @tokens = grep { length() > 0 } @tokens;
        return @tokens;
    }
    "Just give me until tomorrow. I'll think of something."
}
