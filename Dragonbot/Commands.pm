package Commands
{
    # use strict;
    # use warnings;
    our %commands;

    my $httyd = sub {
        use integer;
        # my $HTTYD2 => 1_402_631_999;
        my $HTTYD3 = 1_466_135_999;
        $HTTYD3 += 3600 if (localtime time)[8];
        my $time_left = $HTTYD3 - time;
        my @times = (
            [ day    =>  $time_left / (24 * 3600) ],
            [ hour   => ($time_left /  3600) % 24 ],
            [ minute => ($time_left /  60) % 60   ],
            [ second =>  $time_left %  60         ],
        );

        my $format = sub {
            my ($type, $amount) = @_;
            if    ($amount == 0) { return                    }
            elsif ($amount == 1) { return "$amount $type"    }
            else                 { return "$amount ${type}s" }
        };

        my $message = join(q(, ),
            grep { $_ }
            map  { $format->( @{$_} ) } @times
        );

        return $message . ' left until HTTYD3.';
    };

    my $winner = sub {
        my $sub = (caller(0))[3];
        die "$sub unimplemented";
        # Once Hitbox provides documentation for the chat,
        # this will pick a random user and say their name.
    };

    my $fenris = sub {
        my $message = shift;
        my @sentence = map ucfirst lc, split /\s+/, $message;
        return @sentence if wantarray;
        return join ' ', @sentence;
    };

    my $smasho = sub {
        my $message  = shift;
        my @sentence = map lc, split /\s+/, $message;
        for my $word (@sentence)
        {
            my @letters = split //, $word;
            for (0 .. int(rand(sqrt(@letters))))
            {
                my $r1 = int rand @letters;
                my $r2 = int rand @letters;
                $r2    = int rand $r1 while (abs $r1 - $r2 > 2);
                @letters[$r1, $r2] = @letters[$r2, $r1];
            }
            $word = join '', @letters;
        }
        return join ' ', @sentence;
    };

    %commands = (
        fenris => $fenris, 
        httyd  => $httyd, 
        smasho => $smasho,
        winner => $winner, 
    );

    "Great, my first girlfriend is a dragon."
} # End of Commands
