use Test::More tests => 6;
use lib '../';
use_ok('Commands') or BAIL_OUT;

my $httyd = Commands::getcom('httyd');
ok( UNIVERSAL::isa($httyd, 'Command'), 'Commands::getcom() returns a Command object' );
ok( $httyd->is eq 'STATIC', 'Commands->is method' );
ok( $httyd->is_static, 'Commands->is_static method' );
ok(!$httyd->is_filter, 'Commands->is_filter method' );
ok( $httyd->run =~ /left until HTTYD3/, 'Correct output for $httyd->run' );
